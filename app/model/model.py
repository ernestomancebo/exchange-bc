from app.data import Base, engine
from sqlalchemy import Column, DateTime, Float, Integer, String
from sqlalchemy.schema import FetchedValue


class ExchangeSerie(Base):
    __tablename__ = "bc_exchange"

    id = Column(Integer, primary_key=True)
    buy = Column(Float)
    sell = Column(Float)
    date = Column(DateTime)
    currency = Column(String)
    entry_time = Column(DateTime, server_default=FetchedValue())

    def __repr__(self):
        return f"ExchangeSerie(id={self.id!r}, currency={self.currency!r}, buy={self.buy!r}, sell={self.sell!r}, date={self.date!r}"


# Boostrap the tables
Base.metadata.create_all(engine)
