from app.data import session
from app.model.model import ExchangeSerie
from sqlalchemy import func


class RateDao():
    def __init__():
        pass

    def add_exchange_serie(e: ExchangeSerie):
        try:
            session.add(e)
        except:
            session.rollback()
        session.commit()

    def bulk_add_exchange_serie(e: list):
        try:
            session.bulk_save_objects(e)
        except:
            session.rollback()
        session.commit()

    def get_most_recent_exchange():
        most_recent = session.query(
            func.max(ExchangeSerie.date).label('most_recent')).scalar()

        return most_recent
