
from os import environ

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

# USE ENVIRONMENT VARIABLE
engine = create_engine(environ.get["SQLALCHEMY_DATABASE_URI"], client_encoding="utf-8",
                       echo=False, pool_recycle=1800)
session = scoped_session(sessionmaker(
    autocommit=False, autoflush=False, bind=engine))

""":type: sqlalchemy.orm.Session"""
Base = declarative_base()
