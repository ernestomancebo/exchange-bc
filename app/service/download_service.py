from os import path

import requests
from app.service.file_service import FileService


class DownloadService():
    def __init__(self, bc_url: str, file_service: FileService):
        self.bc_url = bc_url
        self.basedir = path.abspath(path.dirname(__file__))
        self.file_service = file_service

    def download_usd_xls(self, usd_xls_path, xls_file_name):
        response = requests.get(f"{self.bc_url}{usd_xls_path}")
        if response.status_code != 200:
            return self.__error_result(
                f"Server returned status code: '{response.status_code}'"), 500
        file_path = self.file_service .save_binary(
            response.content, xls_file_name)

        return file_path
