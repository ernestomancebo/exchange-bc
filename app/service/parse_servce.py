from datetime import date

from app.model.model import ExchangeSerie
from xlrd import open_workbook


class ParseService():
    def __init__(self):
        self.months_es = {'Ene': 1, 'Feb': 2, 'Mar': 3, 'Abr': 4, 'May': 5, 'Jun': 6,
                          'Jul': 7, 'Ago': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dic': 12}

    def parse_dollar_xls_to_series(self, file_path,  sheet_index=0, start_index=3, end_date: date = None):
        wb = open_workbook(file_path)
        sheet = wb.sheet_by_index(sheet_index)

        rates = []
        for row in reversed(range(start_index, sheet.nrows)):
            cell = [c.value for c in row]
            cell[1] = self.str_to_month(cell[1])
            entry_date = self.int_to_date(*cell[:3])

            # Reached the most recent date
            if end_date is not None and entry_date == end_date:
                break

            entry = ExchangeSerie(entry_date, *cell[3:])
            rates.append(entry)

        return rates

    def str_to_month(self, month: str):
        return self.months_es.get(month, None)

    def int_to_date(self, year: int, month: int, day: int):
        return date(year, month, day)
