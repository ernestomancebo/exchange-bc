from os import path, remove


class FileService():
    def __init__():
        pass

    def save_binary(self, data, file_name):
        file_path = path.join(self.basedir, file_name)

        out_file = open(file_path, 'wb')
        out_file.write(data)
        out_file.close

        return file_path

    def delete_file(self, file_name):
        if path.exists(file_name):
            remove(file_name)
